import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Reservas {

	private JFrame frmReservassalnAustralia;
	private JTextField textField_tel;
	private JTextField textField_cont;
	private JTextField textField_jor;
	private JTextField textField_hab;
//	private JLabel lblNJornadas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Reservas window = new Reservas();
					window.frmReservassalnAustralia.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Reservas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmReservassalnAustralia = new JFrame();
		frmReservassalnAustralia.setTitle("Reservas \"Salón Australia\"");
		frmReservassalnAustralia.setAlwaysOnTop(true);
		frmReservassalnAustralia.setBounds(100, 100, 450, 300);
		frmReservassalnAustralia.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmReservassalnAustralia.getContentPane().setLayout(null);
		
		JLabel lblDatosDeContacto = new JLabel("Datos de contacto");
		lblDatosDeContacto.setBounds(0, 0, 442, 15);
		lblDatosDeContacto.setHorizontalAlignment(SwingConstants.CENTER);
		frmReservassalnAustralia.getContentPane().add(lblDatosDeContacto);
		
		JLabel lblNombre = new JLabel("Nombre y apellidos:");
		lblNombre.setFont(new Font("Dialog", Font.BOLD, 10));
		lblNombre.setBounds(10, 27, 132, 15);
		frmReservassalnAustralia.getContentPane().add(lblNombre);
		
		textField_tel = new JTextField();
		textField_tel.setHorizontalAlignment(SwingConstants.CENTER);
		textField_tel.setFont(new Font("Dialog", Font.PLAIN, 10));
		textField_tel.setBounds(140, 52, 180, 19);
		frmReservassalnAustralia.getContentPane().add(textField_tel);
		textField_tel.setColumns(10);
		
		JLabel lblTelfono = new JLabel("Teléfono:");
		lblTelfono.setFont(new Font("Dialog", Font.BOLD, 10));
		lblTelfono.setBounds(10, 54, 69, 15);
		frmReservassalnAustralia.getContentPane().add(lblTelfono);
		
		textField_cont = new JTextField();
		textField_cont.setHorizontalAlignment(SwingConstants.CENTER);
		textField_cont.setFont(new Font("Dialog", Font.PLAIN, 10));
		textField_cont.setColumns(10);
		textField_cont.setBounds(140, 25, 180, 19);
		frmReservassalnAustralia.getContentPane().add(textField_cont);
		
		JLabel lblDescripcinDelEvento = new JLabel("Descripción del evento");
		lblDescripcinDelEvento.setHorizontalAlignment(SwingConstants.CENTER);
		lblDescripcinDelEvento.setBounds(0, 83, 442, 15);
		frmReservassalnAustralia.getContentPane().add(lblDescripcinDelEvento);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setFont(new Font("Dialog", Font.BOLD, 10));
		lblFecha.setBounds(10, 110, 45, 15);
		frmReservassalnAustralia.getContentPane().add(lblFecha);
		
		JSpinner spinner = new JSpinner();
		spinner.setToolTipText("Elige tu fecha");
		spinner.setFont(new Font("Dialog", Font.BOLD, 10));
		spinner.setModel(new SpinnerDateModel(new Date(1573599600000L), null, null, Calendar.DAY_OF_YEAR));
		spinner.setBounds(96, 110, 98, 15);
		frmReservassalnAustralia.getContentPane().add(spinner);
		
		JLabel lblTipo = new JLabel("Tipo:");
		lblTipo.setFont(new Font("Dialog", Font.BOLD, 10));
		lblTipo.setBounds(227, 109, 33, 16);
		frmReservassalnAustralia.getContentPane().add(lblTipo);
		
		JComboBox comboBox_eve = new JComboBox();
		comboBox_eve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(comboBox_eve.getSelectedItem().toString() == "Congreso") {
					//System.out.println("Hay que hacer activar campos");
					activatefields(); 
					} else {
						desactivatefields();
					}
			}
		});
		comboBox_eve.setFont(new Font("Dialog", Font.BOLD, 10));
		comboBox_eve.setToolTipText("Selecciona tu evento");
		comboBox_eve.setModel(new DefaultComboBoxModel(new String[] {"Banquete", "Jornada", "Congreso", "Boda"}));
		comboBox_eve.setBounds(323, 110, 93, 18);
		frmReservassalnAustralia.getContentPane().add(comboBox_eve);
		
		JLabel lblNmeroDeAsistentes = new JLabel("Nº asistentes:");
		lblNmeroDeAsistentes.setFont(new Font("Dialog", Font.BOLD, 10));
		lblNmeroDeAsistentes.setBounds(10, 157, 85, 15);
		frmReservassalnAustralia.getContentPane().add(lblNmeroDeAsistentes);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setToolTipText("Número de asistentes");
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"1 - 10", "10 - 20", "20 - 50", "50 - 100", "Mas de 100"}));
		comboBox.setFont(new Font("Dialog", Font.BOLD, 10));
		comboBox.setBounds(132, 155, 62, 18);
		frmReservassalnAustralia.getContentPane().add(comboBox);
		
		JLabel lblTipoDeCocina = new JLabel("Tipo de cocina:");
		lblTipoDeCocina.setFont(new Font("Dialog", Font.BOLD, 10));
		lblTipoDeCocina.setBounds(227, 157, 93, 15);
		frmReservassalnAustralia.getContentPane().add(lblTipoDeCocina);
		
		JComboBox comboBox_coc = new JComboBox();
		comboBox_coc.setModel(new DefaultComboBoxModel(new String[] {"No precisa", "Bufé", "Carta", "Menú", "Cita con Chef"}));
		comboBox_coc.setToolTipText("Selecciona el tipo de cocina");
		comboBox_coc.setFont(new Font("Dialog", Font.BOLD, 10));
		comboBox_coc.setBounds(323, 155, 93, 18);
		frmReservassalnAustralia.getContentPane().add(comboBox_coc);
		
		JLabel lblNJornadas = new JLabel("Nº jornadas:");
		lblNJornadas.setFont(new Font("Dialog", Font.BOLD, 10));
		lblNJornadas.setBounds(10, 202, 85, 15);
		frmReservassalnAustralia.getContentPane().add(lblNJornadas);
		
		textField_jor = new JTextField();
		textField_jor.setToolTipText("Elige los dias de tu jornada");
		textField_jor.setEnabled(false);
		textField_jor.setHorizontalAlignment(SwingConstants.CENTER);
		textField_jor.setFont(new Font("Dialog", Font.PLAIN, 10));
		textField_jor.setColumns(10);
		textField_jor.setBounds(161, 200, 33, 19);
		frmReservassalnAustralia.getContentPane().add(textField_jor);
		
		JLabel lblNHabitaciones = new JLabel("Nº habitaciones:");
		lblNHabitaciones.setFont(new Font("Dialog", Font.BOLD, 10));
		lblNHabitaciones.setBounds(227, 202, 85, 15);
		frmReservassalnAustralia.getContentPane().add(lblNHabitaciones);
		
		textField_hab = new JTextField();
		textField_hab.setToolTipText("Elige las habitaciones");
		textField_hab.setHorizontalAlignment(SwingConstants.CENTER);
		textField_hab.setFont(new Font("Dialog", Font.PLAIN, 10));
		textField_hab.setEnabled(false);
		textField_hab.setColumns(10);
		textField_hab.setBounds(383, 200, 33, 19);
		frmReservassalnAustralia.getContentPane().add(textField_hab);
	

	}

	protected void activatefields() {
		// TODO Auto-generated method stub
//		Reservas.lblNJornadas.setEnabled(true);
		textField_hab.setEnabled(true);
		textField_jor.setEnabled(true);
		
	}
	protected void desactivatefields() {
		// TODO Auto-generated method stub
//		Reservas.lblNJornadas.setEnabled(true);
		textField_hab.setEnabled(false);
		textField_jor.setEnabled(false);
		
	}
}
